<?php

use SebastianBergmann\Environment\Console;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     // return view('welcome');
// });

Route::get('/compress', function () {
    $string = request()->get('text');
    if (preg_match("/php/i", "PHP is the web scripting language of choice.")) {
        $reg_pattern = '/([a-f])\\1*/mi';
        preg_match_all($reg_pattern, $string, $matches);
        function encode($str)
        {
            $char_arr = str_split($str);
            $char_count = count($char_arr);
            if ($char_count < 3) {
                return $str;
            } else {
                return "{$char_arr[0]}{$char_count}";
            };
        };
        $encoded_string = strtolower(implode(array_map('encode', $matches[0])));
        return response()->json(['string' => "${encoded_string}"], 200);
        // return response()->json(['string' => "${string}"], 200);
    } else {
        $encoded_text = "Вхождение не найдено.";
    }
});
Route::get('/decompress', function () {
    $string = request()->get('text');
    $reg_pattern = ("/([a-f])(\d+)/i");
    preg_match_all($reg_pattern, $string, $matches);
    function create_reg($reg){
        return "/{$reg}/";
    };
    $reg_dec_pattern = array_map('create_reg', $matches[0]);
    $reg_rep_pattern =array();
    foreach ($matches[1] as $index=>$match){
        $replace = str_repeat($match, $matches[2][$index]);
        array_push($reg_rep_pattern, $replace);
    };
    $decoded_string = preg_replace($reg_dec_pattern, $reg_rep_pattern, $string);
    return response()->json(['string' => "${decoded_string}"], 200);
});

